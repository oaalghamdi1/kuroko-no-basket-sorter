# kuroko no basket sorter
<!DOCTYPE html> = $0
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <title>Haikyuu Sorter</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" href="Images/volleyball.png">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-180495739-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-180495739-1');
</script></head>
<body>

<div id="intro">
    <div style="text-align: center;">
        <img id="logo" src="Images/haikyuu.png">
    </div>
<div id="dscr">
    <h1 class="infoHeader" style="padding-bottom: 3vh;">What is this and how does it work?</h1>
    This is a character sorter designed for the series "Kuroko No Basket!!". You will be given an option of two characters, 
    and all you have to do is simply select the character who you like more. Don't worry if the character
    on the left side stays the same for a while, the sorter is intentionally designed to work like that. Also, you can undo your previous selection by clicking the red arrow button next to the characters. 
    After a while, you'll arrive on a page that will display the results of the sorter and the order in which you ranked the characters. You can share your results by either copying the URL of your results page 
    or by simply just taking a screen shot of the results. To start off, select which sorter you want to do. You can either sort to get your top 10 only, or sort to get the results for 
    all characters. <b style="color: red;">Sorting for your top 10 takes less time to do, but you will not see anything past 10!</b>
</div>
<div id="sorters" class="btn-group">
    <h1 class="infoHeader" style="margin-bottom: 3.5vh;">Select a Sorter</h1>
    <div id="options">
    <button class="button" id="quick">Top 10</button>
    <button class="button" id="all">All</button>
</div>
</div>
</div>
<div id="characterSorter"> 
    <h1>Pick the character you like more!</h1>
    <div id="container">   
        <img class="character" id="left" src="https://haikyuu-sorter.netlify.app/Images/Ennoshita.png"> 
        <img id="back" src="Images/back.png">
        <img class="character" id="right" src="https://haikyuu-sorter.netlify.app/Images/Kenma.png"></div>
</div>


<script src="main.js"></script></body></html>
